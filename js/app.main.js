


//Esse é o arquivo de JS que vai o codigo da aplicação
if (!(window.console && console.log)) {
    (function() {
        var noop = function() {};
        var methods = ['assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error', 'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log', 'markTimeline', 'profile', 'profileEnd', 'markTimeline', 'table', 'time', 'timeEnd', 'timeStamp', 'trace', 'warn'];
        var length = methods.length;
        var console = window.console = {};
        while (length--) {
            console[methods[length]] = noop;
        }
    }());
}

// Retorna o o diretorio atual ex.: http://xxx.com/dir/awesome/index.html -> /dir/awesome
function urlPath() {
    var path = String(window.location.pathname).split('/');
    path.pop();
    return path.join('/');
}

/**
 * jQuery.browser.mobile (http://detectmobilebrowser.com/)
 * jQuery.browser.mobile will be true if the browser is a mobile device
 **/
(function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);

var currentProp = 1;
var propsEls = [
    [34, 35, 38, 39, 41, 42, 43, 45, 48, 50, 57],
    [44, 60, 61, 62, 63, 64, 65, 66, 95, 96, 97],
    [67, 68, 69, 70, 71, 72, 50, 98, 99]
]

var wow = new WOW({
    offset: 150,
    mobile: false
});

$(function() {
    setTimeout(function(){ wow.init() }, 1000);

    //temporário, usado para posicionar
    // $("#container .imagem").draggable({
    //     start: function( event, ui ) {
    //         $(event.target).addClass('drag');
    //     },
    //     stop: function( event, ui ) {
    //         $(event.target).removeClass('drag');
    //         console.log( $(event.target).get(0).id );
    //     },
    // });

    $("#img33").click(function(event) {
        currentProp++;
        if( currentProp > propsEls.length ) currentProp = 1
        trocarPropostas();
    });

    $("#img32").click(function(event) {
        currentProp--;
        if( currentProp <= 0 ) currentProp = propsEls.length
        trocarPropostas();
    });

    $("#img100").click(function(event) {
        currentProp = 1;
        trocarPropostas();
    });

    $("#img102").click(function(event) {
        currentProp = 2;
        trocarPropostas();
    });

    $("#img104").click(function(event) {
        currentProp = 3;
        trocarPropostas();
    });

    $('.link-video').fancybox({
        padding: 0,
        helpers : {
            media : {}
        }
    });
});

function trocarPropostas() {
    $("body").animate({scrollTop: 2700}, 800);

    for (var i = propsEls.length - 1; i >= 0; i--) {
        var itens = propsEls[i];

        if( i != currentProp-1 ){
            for (var j = itens.length - 1; j >= 0; j--) {
                var el = "#img" + itens[j];
                $(el).removeClass('animated').hide();
            };
        } else {
            for (var j = itens.length - 1; j >= 0; j--) {
                var el = "#img" + itens[j];
                wow.show( $(el).addClass('animated').show().get(0) );
            };
        }
    };

    wow.scrollCallback();

    $("#img44").stop().fadeIn();
    $("#img50,#img59,#img100,#img101,#img102,#img103,#img104,#img105").removeClass('hidden').stop().fadeOut();

    if( currentProp == 3 || currentProp == 1 ){
        $("#img50").stop().fadeIn();
        $("#img59").stop().fadeIn();

        if( currentProp == 3 ){
            $("#img44").stop().fadeOut();

            $("#img100").stop().fadeIn();
            $("#img102").stop().fadeIn();
            $("#img105").stop().fadeIn();

            $(".link-troca").attr('href', 'https://www.youtube.com/embed/QK8h9gmQ9tw?autoplay=1');
        } else {
            $("#img101").stop().fadeIn();
            $("#img102").stop().fadeIn();
            $("#img104").stop().fadeIn();
            $(".link-troca").attr('href', 'https://www.youtube.com/embed/Veh-hQNvHpY?autoplay=1');
        }
    } else {
        $("#img100").stop().fadeIn();
        $("#img103").stop().fadeIn();
        $("#img104").stop().fadeIn();
    }
}

function gerarPosicoes () {
    var str = "";
    $("#container .imagem").each(function(index, el) {
        // if( ! $(this).is(':visible') ) return;

        var pos = $(this).position();
        str += "\t#" + this.id + " { top: " + ( pos.top ) + "px; left: " + pos.left + "px; }\n";

    });
    console.log( str );
}